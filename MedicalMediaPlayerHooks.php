<?php

/**
 * All hooked functions used by MedicalMediaPlayer
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

class MedicalMediaPlayerHooks {


	/**
	 * Tell the wiki that this function will be triggered by tag <playmm>
	 */
	public static function registerPlayMMtag($parser) {
		$parser->setHook('playmm', 'MedicalMediaPlayerHooks::showMMvideo');
		return true;
	}

	/**
	 * Define function that will take care of text enclosed in the new tag
	 */
	public static function showMMvideo($input,$params,$parser) {
				
		// all attributes required
		if( array_key_exists( 'id',$params ) && is_numeric( $params['id'] ) &&
			array_key_exists( 'width',$params ) && is_numeric( $params['width'] ) &&
			array_key_exists( 'height',$params ) && is_numeric( $params['height'] ) ) {
		
			$id = $params['id'];
			$width = $params['width'];
			$height = $params['height'];
			return "<iframe src='https://www.medicalmedia.eu/cs/detail/embed/$id' width='".$width."px' height='".$height."px' frameborder='0' scrolling='no'></iframe>\n";
		}
		else {
			return '';
		}

	}

}