# MedicalMediaPlayer

Mediawiki extension.

## Description

* Version 1.0
* Embedding streams from [http://www.medicalmedia.eu](www.medicalmedia.eu) to wiki articles.
* Code for embedding the movie can be found at [http://www.medicalmedia.eu](www.medicalmedia.eu).
* All attributes are required.
* Example:

```xml
<playmm id="454" width="640" height="360"></playmm>
```

## Instalation

* Make sure you have MediaWiki 1.29+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'MedicalMediaPlayer' )`;

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2018 First Faculty of Medicine, Charles University
